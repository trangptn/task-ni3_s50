const mongoose=require('mongoose');
const db=require('../models');

const productModel=db.product;

const getAll= async (req,res)=>{
    try{
        const result= await productModel.find().lean();
        return res.status(200).json({
            message:"Successful !",
            data:result
        })
    }
    catch(error)
    {
        return res.status(500).json({
            message:"Invalid error !"
        })
    }
}

const getProductById= async (req,res)=>{
    const _id=req.params.id;
    if(!mongoose.Types.ObjectId.isValid(_id)){
        return res.status(400).json({
            message:"Id invalid"
        })
    }
    try{
        const result=await productModel.findById(_id).lean();
        if(result)
            {
                return res.status(200).json({
                    message:"Successful !",
                    data:result
                })
            }
            else
            {
                return res.status(404).json({
                    message:"Couldn't find product !"
                })
            }
    }
    catch(error)
    {
        return res.status(500).json({
            message:"Invalid error !"
        })
    }
}

const createProduct= async (req,res)=>{
    const 
    {
        reqName,
        reqPrice
    }=req.body;
    console.log(reqName);
    if(!reqName)
        {
            return res.status(400).json({
                message:"Name invalid !"
            })
        }
    if(!reqPrice){
        return res.status(400).json({
            message:"Price invalid !"
        })
    }
    try{
        const result= await productModel.create({
            name:reqName,
            price:reqPrice
        });
        return res.status(201).json({
            message:"Create product successfull !",
            data:result
        })
    }
    catch(error){
        return res.status(500).json({
            message:"Invalid error"
        })
    }
}

const updateProduct= async (req,res)=>{
    const _id=req.params.id;
    if(!mongoose.Types.ObjectId.isValid(_id))
        {
            return res.status(400).json({
                message:"Id invalid !"
            })
        }
    const { name, price}=req.body;

    if(name && name===""){
        return res.status(400).json({
            message:"Name invalid !"
        })
    }

    if(price && price==="")
        {
           return  res.status(400).json({
                message:"Price invalid !"
            })
        }
    
        try{
            let updateProduct={};
            if(name){
                updateProduct.name=name;
            }
            if(price){
                updateProduct.price=price;
            }
            const result= await productModel.findByIdAndUpdate(_id,updateProduct);
            if(result)
                {
                    return res.status(200).json({
                        message:"Update successfull !",
                        data:result
                    })
                }
            else{
                return res.status(400).json({
                    message:"Couldn't update product type !"
                })
            }
        }
        catch(error)
        {
            console.log(error);
            return res.status(500).json({
                message:"Invalid error !"
            })
        }
    
}

const deleteProductById=async (req,res)=>{
    const _id=req.params.id;
    if(!mongoose.Types.ObjectId.isValid(_id)){
        return res.status(400).json({
            message:"Id invalid !"
        })
    }

    try{
        const result=await productModel.findByIdAndRemove(_id);
        if(result){
            return res.status(200).json({
                message:"Delete successful !"
            })
        }
        else
        {
            return res.status(404).json({
                message:"Couldn't find product"
            })
        }
        
    }
    catch(error)
    {
        return res.status(500).json({
            message:"Invalid error"
        })
    }
}

module.exports={
    getAll,
    getProductById,
    createProduct,
    updateProduct,
    deleteProductById
}
