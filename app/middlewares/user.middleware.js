const db = require("../models");
const jwt = require('jsonwebtoken');

const User = db.user;
const Role = db.role;

const verifyToken = async (req, res, next) => {
    console.log("verify token ...");
    let token = req.headers['x-access-token'];
    console.log(token);
    if (!token) {
        return res.status(401).send({
            message: "Không tìm thấy x-access-token!"
        });
    }

    const secretKey = process.env.JWT_SECRET;

    const verified = await jwt.verify(token, secretKey);
    console.log(verified);
    if (!verified) {
        return res.status(401).send({
            message: "x-access-token không hợp lệ!"
        });
    }

    const user = await User.findById(verified.id).populate("roles");
    console.log(user);
    req.user = user;
    next();    
}


const checkUser = (req, res, next) => {
    console.log("check user ...");
    const userRoles = req.user.roles;
    if (userRoles) {
        for (let i=0; i<userRoles.length; i++) {
            if (userRoles[i].name == 'admin' ) {
                console.log("Authorized!");
                next();
                return;
            }
        }
    }

    console.log("Unauthorized");
    return res.status(401).send({
        message: "Bạn không có quyền truy cập!"
    });

    /*
    User.findById(req.userId)
    .then((data) => {
        console.log("verify ok");
        console.log(data);
        Role.find({_id : {$in : data.roles}})
        .then((roles) => {    
            console.log(roles);
            for (let i=0; i<roles.length; i++) {
                if (roles[i].name == 'admin' ) {
                    console.log("authorized!");
                    next();
                    return;
                }
            }
            res.status(401).send({
                message: "Unauthorized!"
            })
            return;
        })
        .catch((err) => {
            res.status(401).send({
                message: "Unauthorized!"
            })
            return;
        })

    })
    .catch((err) => {
        console.log("verify nok");
        console.log(err);
        return;
    })
    
    /*
    .exec((err, user) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }
        console.log("verify role:");
        console.log(user);
    })*/
}

module.exports = {
    verifyToken,
    checkUser
}