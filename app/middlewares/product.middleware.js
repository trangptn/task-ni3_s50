const jwt=require('jsonwebtoken');
const db=require("../models");

const verifyToken=async (req,res,next)=>{
    const authHeader=req.headers['authorization'];
    const token=authHeader && authHeader.split('  ')[1];
    if(!token)
        {
            return res.status(401).json({
                message:"Token not found"
            })
        }

        const verified=jwt.verify(token,process.env.JWT_SECRET,(err,decoded)=>{
        if(err)
            {
                return res.status(403).json({
                    message:`Failed to authenticate token ${err.message}`
                })
            }
            return decoded;
    })
    const user = await db.user.findById(verified.id).populate("roles");
    req.user=user;
    next();
}

const checkUser= async (req,res,next)=>{
    // console.log(req.user);
    if(!req.user){
        return res.status(403).json({
            message:"Access denied. Admin permission required !"
        });
    }
    else{
    for (let i=0; i<req.user.roles.length; i++) {
        if (req.user.roles[i].name != 'admin' ) {
            return res.status(403).json({
                message:"Access denied. Admin permission required !"
            });
        }
    }
}
    next();
}

module.exports={verifyToken,checkUser};