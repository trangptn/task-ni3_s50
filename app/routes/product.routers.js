const express = require("express");

const route = express.Router();

const path=require('path');

route.use(express.static(__dirname+'/app/views'));

const productMiddleWare = require("../middlewares/product.middleware");
const productController = require("../controllers/product.controller");

route.get("/",productMiddleWare.verifyToken,productController.getAll);
route.post("/",[productMiddleWare.verifyToken,productMiddleWare.checkUser] ,productController.createProduct);
route.get("/:id",[productMiddleWare.verifyToken,productMiddleWare.checkUser],productController.getProductById);
route.put("/:id", [productMiddleWare.verifyToken, productMiddleWare.checkUser], productController.updateProduct);
route.delete("/:id",[productMiddleWare.verifyToken,productMiddleWare.checkUser] , productController.deleteProductById);

module.exports = route;